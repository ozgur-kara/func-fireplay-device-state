import base64
import datetime
import json

from google.cloud import bigquery


def save_to_bigquery(event, context):
    if 'data' in event:
        rec_data = base64.b64decode(event['data']).decode('utf-8')
    else:
        rec_data = {}

    rec_data_dic = json.loads(rec_data)
    serial = event['attributes']['deviceId']
    now = datetime.datetime.now()

    payload = get_or_default(rec_data_dic, "payload", {'serial': serial})
    payload["event_date"] = format_date(now.__str__())
    payload["local_date"] = get_or_default(rec_data_dic, "local_date", None)
    payload["lat"] = get_or_default(rec_data_dic, "lat", None)
    payload["lng"] = get_or_default(rec_data_dic, "lng", None)
    payload["app_version"] = get_or_default(rec_data_dic, "app_version", None)

    bigquery_client = bigquery.Client()

    dataset_ref = bigquery_client.dataset('display_ds')
    table_ref = dataset_ref.table('device_state')

    table = bigquery_client.get_table(table_ref)
    rows_to_insert = [
        {
            u"event_date": payload["event_date"],
            u"serial": payload["serial"],
            u"lat": payload["lat"],
            u"lng": payload["lng"],
            u"app_version": payload["app_version"]
        }
    ]

    errors = bigquery_client.insert_rows_json(table, rows_to_insert)
    if errors == []:
        print("New rows have been added.")
    else:
        print("Encountered errors while inserting rows: {}".format(errors))


def get_or_default(json_value, key, default_value):
    val = None
    if key in json_value:
        val = json_value[key]

    if val is None or val == '':
        return default_value
    else:
        return val


def format_date(date_string):
    event_date = datetime.datetime.strptime(date_string, "%Y-%m-%d %H:%M:%S.%f")
    return event_date.strftime("%Y-%m-%d %H:%M:%S UTC")
